# Librairies utilisées
from tqdm.notebook import tqdm
import einops
import math
import torch
from torch import nn



# Create a class that make the Patchification and the Linear Embedding 
class EmbedPatch(nn.Module):
    def __init__(self, patch_size, n_channels, device, latent_size, batch_size):
        super(EmbedPatch, self).__init__()
        self.latent_size = latent_size
        self.patch_size = patch_size
        self.n_channels = n_channels
        self.device = device
        self.batch_size = batch_size
        self.input_size = self.patch_size * self.patch_size * self.n_channels

        self.linearProjection = nn.Linear(self.input_size, self.latent_size)

        # Random initialization of [class] token that is prepended to the linear projection vector.
        self.class_token = nn.Parameter(torch.randn(self.batch_size, 1, self.latent_size)).to(self.device)

        # Positional embedding
        self.pos_embedding = nn.Parameter(torch.randn(self.batch_size, 1, self.latent_size)).to(self.device) #### !!!!!! A COMPRENDRE 
        #self.pos_embedding = nn.Parameter(torch.arange(self.batch_size, (im_dim[0]*im_dim[1])/(patch_size**2))).to(self.device)


    def forward(self, input_data):
        #print(type(input_data))

        if len(input_data.size()) == 3:
            input_data = torch.unsqueeze(input_data, 1)

        # Re-arrange image into patches.
        patches = einops.rearrange(
            input_data, 'b c (h h1) (w w1) -> b (h w) (h1 w1 c)', h1=self.patch_size, w1=self.patch_size)

        linear_projection = self.linearProjection(patches).to(self.device)

        b, n, _ = linear_projection.shape

       #print(linear_projection.shape)

        # Prepend the [class] token to the original linear projection
        linear_projection = torch.cat((self.class_token, linear_projection), dim=1)
        pos_embed = einops.repeat(self.pos_embedding, 'b 1 d -> b m d', m=n+1) # n  + 1 (n for the number of patches et 1 for the cls (class token))

        # Add positional embedding to linear projection
        linear_projection += pos_embed

        return linear_projection
    


class EncoderBlock(nn.Module):
    def __init__(self, latent_size, num_heads, device, dropout):
        super(EncoderBlock, self).__init__()

        self.latent_size = latent_size
        self.num_heads = num_heads
        self.device = device
        self.dropout = dropout

        # Normalization layer for both sublayers
        self.norm = nn.LayerNorm(self.latent_size)
        
        # Multi-Head Attention layer
        self.multihead = nn.MultiheadAttention(
            self.latent_size, self.num_heads, batch_first=True, dropout=self.dropout)          

        # MLP_head layer in the encoder. I use the same configuration as that 
        # used in the original VitTransformer implementation. The ViT-Base
        # variant uses MLP_head size 3072, which is latent_size*4.
        self.enc_MLP = nn.Sequential(
            nn.Linear(self.latent_size, self.latent_size*4),
            nn.GELU(),
            nn.Dropout(self.dropout),
            nn.Linear(self.latent_size*4, self.latent_size),
            nn.Dropout(self.dropout)
        )

    def forward(self, embedded_patches):

        # First sublayer: Norm + Multi-Head Attention + residual connection.
        # We take the first element ([0]) of the returned output from nn.MultiheadAttention()
        # because this module returns 'Tuple[attention_output, attention_output_weights]'. 
        # Refer to here for more info: https://pytorch.org/docs/stable/generated/torch.nn.MultiheadAttention.html 
        firstNorm_out = self.norm(embedded_patches)
        attention_output = self.multihead(firstNorm_out, firstNorm_out, firstNorm_out)[0]

        # First residual connection
        first_added_output = attention_output + embedded_patches

        # Second sublayer: Norm + enc_MLP (Feed forward)
        secondNorm_out = self.norm(first_added_output)
        ff_output = self.enc_MLP(secondNorm_out)

        # Return the output of the second residual connection
        return ff_output + first_added_output
    


class Upsample(nn.Sequential):
    """Upsample module.

    Args:
        scale (int): Scale factor. Supported scales: 2^n and 3.
        num_feat (int): Channel number of intermediate features.
    """

    def __init__(self, scale, num_feat):
        m = []
        if (scale & (scale - 1)) == 0:  # scale = 2^n
            for _ in range(int(math.log(scale, 2))):
                m.append(nn.Conv2d(num_feat, 4 * num_feat, 3, 1, 1))
                m.append(nn.PixelShuffle(2))
        elif scale == 3:
            m.append(nn.Conv2d(num_feat, 9 * num_feat, 3, 1, 1))
            m.append(nn.PixelShuffle(3))
        else:
            raise ValueError(f'scale {scale} is not supported. ' 'Supported scales: 2^n and 3.')
        super(Upsample, self).__init__(*m)



class VitTransformer(nn.Module):
    def __init__(self, num_encoders=12, latent_size=64, device='cpu', final_dim=64*64, dropout=0.0, patch_size=8,
                 n_channels=2, batch_size=40, num_heads=8):
        super(VitTransformer, self).__init__()
        self.num_encoders = num_encoders
        self.latent_size = latent_size
        self.device = device
        self.final_dim = final_dim
        self.dropout = dropout
        self.patch_size = patch_size
        self.device = device
        self.n_channels = n_channels
        self.batch_size = batch_size
        self.num_heads = num_heads

        ##########################################################################################
        ########################## PATCHIFICATION AND LINEAR EMBEDDING ###########################
        self.EmbedPatchStack = EmbedPatch(patch_size=self.patch_size, 
                                          n_channels=self.n_channels, 
                                          device=self.device, 
                                          latent_size=latent_size, 
                                          batch_size=self.batch_size)

        ##########################################################################################
        ################################## TRANSFORMER ENCODER ###################################
        # Create a stack of encoder layers
        self.EncoderStack = nn.ModuleList([EncoderBlock(latent_size=latent_size, 
                                                        num_heads=self.num_heads, 
                                                        device=self.device, 
                                                        dropout=self.dropout) 
                                            for i in range(self.num_encoders)])

        ##########################################################################################
        ################################## IMAGE RECONSTRUCTION ##################################
        self.upscale = self.patch_size
        self.num_features = 64
        self.conv_before_upsample = nn.Sequential(nn.Conv2d(in_channels=self.latent_size, out_channels=self.num_features, 
                                                            kernel_size=3, stride=1, padding=1),
                                                  nn.LeakyReLU(inplace=True))
        self.upsample = Upsample(self.upscale, self.num_features)
        self.conv_last = nn.Conv2d(in_channels=self.num_features, out_channels=1, kernel_size=3, stride=1, padding=1)
        

    def forward(self, test_input):

        # Apply input embedding (patchify + linear projection + position embeding)
        # to the input image passed to the model
        enc_output = self.EmbedPatchStack(test_input)

        # Loop through all the encoder layers
        for enc_layer in self.EncoderStack:
            enc_output = enc_layer.forward(enc_output)

        # Extract the output embedding information of the patches token
        patches_token_embedding = enc_output[:, 1:]
        
        patches_token_embedding = torch.unflatten(patches_token_embedding, 1, (8, 8))
        patches_token_embedding = torch.permute(patches_token_embedding, (0, 3, 1, 2))

        y = self.conv_before_upsample(patches_token_embedding)
        y = self.conv_last(self.upsample(y))
        y = torch.squeeze(y)

        return y