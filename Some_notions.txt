-   Relative positional Bias :  used to introduce positional information in the 
the attention computation. This allows the model to learn on the spatial relations 
between the windows (or patches). This is more useful in the context of SW-MSA.

-   Positional embedding 

-   The operation "Broadcasting" done to obtain the relative positional bias is explained
here https://numpy.org/doc/stable/user/basics.broadcasting.html 

-   The "register_buffer(parm_name, parm_value)" is added to the architecture of a model 
to register a parameter to the buffer (set of parameters that are not updated during the training) 




- view()


- Pour le modèle SUNET, on peut :
    + changer la dimension du embedding à 64 ou 32
    + patch size par 8
    + augmenter le num_heads
    + changer le learning rate