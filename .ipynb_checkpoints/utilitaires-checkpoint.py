# Librairies utilisées
from tqdm.notebook import tqdm
import os
from tempfile import TemporaryDirectory
import numpy as np
import torch
from torchmetrics import PeakSignalNoiseRatio 
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
from sklearn.model_selection import train_test_split
import time

from matplotlib import cm
from matplotlib.colors import ListedColormap
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt

from os import listdir
from os.path import isfile, join
import SimpleITK 
import skimage.io as io

from torch.optim import lr_scheduler

from Models_Architectures import VisionTransformer


################################################################## Loading data 
def load_data(repository_name, type_data) : 
    path = "../data/" + repository_name + "/"
    folders = [f for f in listdir(path) if not isfile(join(path, f))]
    X = []
    for folder in folders : 
        path_small = path + folder  + "/"
        files = [f for f in listdir(path_small) if (isfile(join(path_small, f)) & f.endswith(f"{type_data}.mhd"))]
        for file in files : 
            X.append(io.imread(path_small + file, plugin='simpleitk'))
    return X



################################################################## Computing PSNR
def psnr(img1, img2, max_pixel=255.0):
    """
    Compute the PSNR between two images.
    
    Args:
    img1 (Tensor): the first image
    img2 (Tensor): the second image
    max_pixel (float): the maximum pixel value (255 for 8-bit images)

    Returns:
    float: the PSNR value
    """
    mse = F.mse_loss(img1, img2)
    if mse == 0:
        return float('inf')
    return 20 * torch.log10(max_pixel / torch.sqrt(mse))


################################################################## Create a function that compute the PSNR
def psnr_from_model(dataloader, ind, model_path, model):
    psnr = PeakSignalNoiseRatio()
    model = model
    model.load_state_dict(torch.load(model_path))
    model.eval()
    for X, y in dataloader : 
        with torch.no_grad(): 
            pred = model(X)
            break
    pred_im = pred[ind]
    high_im = y[ind]
    return psnr(high_im, pred_im)

def psnr_min_mean_max(dataloader, model_path, model, batch_size, ct = False):
    psnr = PeakSignalNoiseRatio()
    psnr_list = []
    model = model
    model.load_state_dict(torch.load(model_path))
    model.eval()
    for X, y in dataloader : 
        with torch.no_grad():
            pred = model(X)            
            for ind in range(batch_size):
                predict = pred[ind, :, :]
                label = y[ind, :, :]
                psnr_list.append(psnr(label, predict).item())            
    return (min(psnr_list), sum(psnr_list)/len(psnr_list), max(psnr_list))



def affiche_images(low_image, pred_image, high_image, ct=None):
    ##### Une mode de CMP############
    top = cm.get_cmap('viridis', 64)
    bottom = cm.get_cmap('plasma', 960)
    newcolors = np.vstack((top(np.linspace(0, 1, 64)),
                        bottom(np.linspace(1, 0, 960))))
    newcmp = ListedColormap(newcolors, name='MonteCarlo')

    fig1 = plt.figure(constrained_layout=True, figsize=(9, 3))
    spec = gridspec.GridSpec(ncols=4, nrows=1, figure=fig1)
    ######
    if ct is not None:
        ax1 = fig1.add_subplot(spec[0])
        plt.imshow(ct, interpolation=None, cmap='gray')
        plt.ylabel('Une coupe de bassin',fontweight='bold')
        plt.xlabel('CT',fontweight='bold')
        plt.xticks([], [])
        plt.yticks([], [])
    
    ax1a = fig1.add_subplot(spec[1])
    plt.imshow(low_image, interpolation=None, cmap=newcmp)
    #plt.ylabel('Une coupe de bassin',fontweight='bold')
    plt.xlabel('Low Sampling',fontweight='bold')
    plt.xticks([], [])
    plt.yticks([], [])

    ax1b = fig1.add_subplot(spec[2])
    plt.imshow(torch.detach(pred_image), interpolation=None, cmap=newcmp)
    plt.xlabel('Pred Sampling',fontweight='bold')
    plt.xticks([], [])
    plt.yticks([], [])

    ax2 = fig1.add_subplot(spec[3])
    plt.imshow(high_image, interpolation=None, cmap=newcmp)
    plt.xlabel('High Sampling',fontweight='bold')
    # plt.colorbar(aspect=50)
    plt.xticks([], [])
    plt.yticks([], [])
    ###########
    plt.show()


def function_for_comparison(model_path, dataloader, ind, model, CT=False):
    model = model
    model.load_state_dict(torch.load(model_path))
    model.eval()
    for X, y in dataloader:
        with torch.no_grad():
            result = model(X)
        data = X
        label = y
        break
    low_im = data[ind, :, :]
    #print(low_im.shape)
    if CT:
        ct = low_im[1, :, :]
        ct.squeeze_()
        print(ct.shape)
        low_im = low_im[0, :, :]
        print(low_im.shape)
    low_im.squeeze_()
    pred_im = result[ind, :, :]
    high_im = label[ind, :, :]
    affiche_images(low_image=low_im, pred_image=pred_im, high_image=high_im, ct=ct if CT else None)


# Train and Test loop
def train_loop(dataloader, model, loss_fn, optimizer, device):

    model.train()

    for X, y in dataloader:
        # Reset gradients
        optimizer.zero_grad()

        # Forward
        pred = model(X)
        loss = loss_fn(pred, y)

        # Backpropagation
        #torch.autograd.set_detect_anomaly(True)
        loss.backward()
        optimizer.step()
    


def eval_loop(dataloader, model, loss_fn, device):

    model.eval()
    #size = len(dataloader.dataset)
    #num_batches = len(dataloader)
    loss = 0.

    with torch.no_grad():
        for X, y in dataloader:
            pred = model(X)
            loss += loss_fn(pred, y).item()

    #loss /= num_batches
    return loss



def train_model(dataloaders, model, criterion, optimizer,  device, n_epochs, save_path):
    loss_train_list = []
    loss_val_list = []
    for X, _ in dataloaders["val"]:
        break
    list_pred = [X[0,0, :, :]]

    with TemporaryDirectory() as tempdir:
        best_model_params_path = os.path.join(tempdir, 'best_model_params.pt')

        torch.save(model.state_dict(), best_model_params_path)
        best_loss = 1e2

        for t in range(n_epochs):
            print(f"Epoch {t + 1}\n-------------------------------------------------")

            # Train loop
            train_loop(dataloaders["train"], model, criterion, optimizer, device)
            print('Training done !!! \n Evaluation ')

            # Evaluation loop
            loss_train = eval_loop(dataloaders["train"], model, criterion, device)
            loss_train_list.append(loss_train)
            print(f"Training set:  Avg loss: {loss_train: >8f}")

            loss_val = eval_loop(dataloaders["val"], model, criterion, device)
            loss_val_list.append(loss_val)
            print(f"Validation set:   Avg loss: {loss_val: >8f}\n")
            
            if t > 200:
                model.eval()
                with torch.no_grad():
                    pred = model(X)
                list_pred.append(pred[0,:,:])
            
            if loss_val < best_loss : 
                best_loss = loss_val
                torch.save(model.state_dict(), best_model_params_path)
        
        model.load_state_dict(torch.load(best_model_params_path))
        torch.save(model.state_dict(), save_path)
        print("Done!")

    return model, loss_train_list, loss_val_list, list_pred


def plot_result(x, y, labelY):
    range_epochs = range(1, len(x)+1)

    plt.figure(figsize=(10,5))

    # Plotting the train loss evolution
    plt.plot(range_epochs, x, linestyle='-', color='b', label='Train')

    # Plotting the test loss evolution
    plt.plot(range_epochs, y, linestyle='--', color='r', label='Validation')

    # Adding labels and title
    plt.xlabel('Epochs')
    plt.ylabel(labelY)
    plt.title(f'Train and validation {labelY} for each epochs')

    # Adding a legend to the plot
    plt.legend()

    # Set x-axis ticks to all possible values of x
    #plt.xticks(range_epochs)

    # Display the plot
    plt.show()